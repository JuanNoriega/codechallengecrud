package com.codechallenge.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codechallenge.model.User;
import com.codechallenge.repositories.UserRepository;
@Service
public class UserService {
	
	@Autowired
	User userAux;
	@Autowired(required = true)
	private UserRepository userRepository;

	public UserService() {

	}
	
	public List<User> getAllUsers(){
		List<User> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
		
	}
	
	public User getUserById(Long id) {
		return userRepository.findById(id).get();
		//return users.get(id);
	}
	
	public void addUser(User user) {
		userRepository.save(user);
		
		/*user.setId(users.size() + 1);
		users.put(user.getId(), user);
		return user;*/
	}
	
	public void updateUser(User user, Long id) {
		userAux = userRepository.findById(id).get();
		userAux.setFirstName(user.getFirstName());
		userAux.setLastName(user.getLastName());
		userAux.setDateOfBirth(user.getDateOfBirth());
		userAux.setStatus(user.isStatus());
		userAux.setId(id);
		userRepository.deleteById(id);
		userRepository.save(userAux);
		
		/*if(user.getId() <= 0 ) {
			return null;
		}
		users.put(user.getId(), user);
		return user;*/
	}
	
	public void deleteUser(Long id) {
		userAux = userRepository.findById(id).get();
		userAux.setStatus(false);
		userRepository.save(userAux);
		//return userRepository.findById(id).get();
		//users.get(id).setStatus(false);
		//return users.get(id);
	}

}
