package com.codechallenge.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codechallenge.model.User;
import com.codechallenge.services.UserService;

@RestController
@RequestMapping("/Users")
public class UserResource {
	@Autowired
	UserService userService;
	
	@GetMapping(path = "", produces = "application/json")
	public List<User> getAllUsers(){
		return userService.getAllUsers();
	}
	
	@GetMapping(path = "/{id}", produces = "application/json")
	public User getUser(@PathVariable("id") Long id){
		return userService.getUserById(id);
	}
	
	@PostMapping(path = "", consumes = "application/json", produces = "application/json")
	public void addUser(@RequestBody User user) {
		userService.addUser(user);
	}
	@PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
	public void updateUser(@PathVariable("id") Long id, @RequestBody User user) {
		userService.updateUser(user,id);
		//user.setId(id);
		//return userService.updateUser(user);
	}
	@DeleteMapping(path = "/{id}",produces = "application/json")
	public void deleteUser(@PathVariable Long id) {
		userService.deleteUser(id);
	}

}
