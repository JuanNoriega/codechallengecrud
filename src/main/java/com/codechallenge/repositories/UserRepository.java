package com.codechallenge.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.codechallenge.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{ }
